import stripe
stripe.api_key = ""

# getting charges from dispute lists
def get_charges_from_disp():
  disput = stripe.Dispute.list(limit=1)
  disp_list = []

  for i in disput.auto_paging_iter():
    disp_list.append(i['charge'])
  return disp_list

# getting full details of dispute related charges
def get_details_of_charges():
  char_list = []

  for i in get_charges_from_disp():
    charge = stripe.Charge.retrieve(i, )
    char_list.append(charge)
  return char_list

# getting the list of orders from charges
def get_listed_orders_from_charges():
  try:
    list_of_orders = []
    for i in get_details_of_charges():
      list_of_orders.append(i['metadata']['order_id'])
      # list_of_orders.append(i['payment_method_details']['card']['three_d_secure'])
    return list_of_orders
  except(KeyError):
    return list_of_orders

print(get_listed_orders_from_charges())