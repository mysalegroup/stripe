import stripe
from datetime import datetime
stripe.api_key = "" # site

def get_adjustments():
    adj = stripe.BalanceTransaction.list(limit=1, type='adjustment')
    adj_list = []
    for i in adj.auto_paging_iter():
        adj_list.append(i['net'])
    return adj_list

def get_timestamps():
    adj = stripe.BalanceTransaction.list(limit=1, type='adjustment')
    created_stamp_raw = []
    created_stamp_formatted = []
    for i in adj.auto_paging_iter():
        created_stamp_raw.append(i['created'])
    for i2 in created_stamp_raw:
        created_stamp_formatted.append(datetime.utcfromtimestamp(i2).strftime('%Y-%m-%d %H:%M:%S'))
    return created_stamp_formatted

def get_all_losses():
    losses = sum(get_adjustments())
    return losses

adj_time_dict = dict(zip(get_timestamps(), get_adjustments()))

# chargebacks per months

def January_chargebacks():
    January_ch_dic = {key: value for (key, value) in adj_time_dict.items() if '-01-' in key}
    all_January_chargbacks = sum(January_ch_dic.values())
    return all_January_chargbacks

def February_chargebacks():
    February_ch_dic = {key: value for (key, value) in adj_time_dict.items() if '-02-' in key}
    all_February_chargbacks = sum(February_ch_dic.values())
    return all_February_chargbacks

def March_chargebacks():
    March_ch_dic = {key: value for (key, value) in adj_time_dict.items() if '-03-' in key}
    all_March_chargbacks = sum(March_ch_dic.values())
    return all_March_chargbacks

def April_chargebacks():
    April_ch_dic = {key: value for (key, value) in adj_time_dict.items() if '-04-' in key}
    all_April_chargbacks = sum(April_ch_dic.values())
    return all_April_chargbacks

def May_chargebacks():
    May_ch_dic = {key: value for (key, value) in adj_time_dict.items() if '-05-' in key}
    all_May_chargbacks = sum(May_ch_dic.values())
    return all_May_chargbacks

def June_chargebacks():
    June_ch_dic = {key: value for (key, value) in adj_time_dict.items() if '-06-' in key}
    all_June_chargbacks = sum(June_ch_dic.values())
    return all_June_chargbacks

def July_chargebacks():
    July_ch_dic = {key: value for (key, value) in adj_time_dict.items() if '-07-' in key}
    all_July_chargbacks = sum(July_ch_dic.values())
    return all_July_chargbacks

print(f'Chargeback losses in January: {str(January_chargebacks())[1:-2]} AUD')
print(f'Chargeback losses in February: {str(February_chargebacks())[1:-2]} AUD')
print(f'Chargeback losses in March: {str(March_chargebacks())[1:-2]} AUD')
print(f'Chargeback losses in April: {str(April_chargebacks())[1:-2]} AUD')
print(f'Chargeback losses in May: {str(May_chargebacks())[1:-2]} AUD')
print(f'Chargeback losses in June: {str(June_chargebacks())[1:-2]} AUD')
print(f'Chargeback losses in July: {str(July_chargebacks())[1:-2]} AUD')

# print(get_all_losses())