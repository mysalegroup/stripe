import stripe, time
stripe.api_key = "" # site

# getting id list
def get_id_list():
  get_list = stripe.radar.ValueListItem.list(limit=1, value_list='rsl_xxx')
  list_id = []

  for i in get_list.auto_paging_iter():
    list_id.append(i['id'])
  return list_id

list_of_ids = get_id_list()
time.sleep(100)

for i in list_of_ids:
    stripe.radar.ValueListItem.delete(i,)

# print(get_id_list())